# frozen_string_literal: true

# This module is used to return various SaaS related configurations
# which may be overridden in other variants of GitLab

module Gitlab
  module Saas
    def self.com_url
      'https://src.redpoint.games'
    end

    def self.staging_com_url
      'https://staging.src.redpoint.games'
    end

    def self.subdomain_regex
      %r{\Ahttps://[a-z0-9]+\.src\.redpoint\.games\z}.freeze
    end

    def self.dev_url
      'https://dev.src.redpoint.games'
    end

    def self.registry_prefix
      'registry.redpoint.games'
    end

    def self.customer_support_url
      'https://support.gitlab.com'
    end

    def self.customer_license_support_url
      'https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293'
    end

    def self.gitlab_com_status_url
      'https://status.gitlab.com'
    end
  end
end

Gitlab::Saas.prepend_mod
