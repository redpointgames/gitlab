# frozen_string_literal: true

class AddGroupRepositoryViewLevelToNamespaces < ActiveRecord::Migration[6.0]
  include Gitlab::Database::MigrationHelpers

  DOWNTIME = false

  def up
    add_column(:namespaces, :repository_view_level, :integer) # rubocop:disable Migration/AddColumnsToWideTables
    change_column_default(:namespaces,
                          :repository_view_level,
                          ::Gitlab::Access::REPORTER_VIEW_ACCESS)
  end

  def down
    remove_column(:namespaces, :repository_view_level)
  end
end
